package ru.kuzin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.kuzin.tm.api.endpoint.IAuthEndpoint;
import ru.kuzin.tm.api.endpoint.IDomainEndpoint;
import ru.kuzin.tm.api.service.IPropertyService;
import ru.kuzin.tm.dto.request.*;
import ru.kuzin.tm.marker.SoapCategory;
import ru.kuzin.tm.service.PropertyService;

@Category(SoapCategory.class)
public class DomainEndpointTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IAuthEndpoint AUTH_ENDPOINT_CLIENT = IAuthEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final IDomainEndpoint DOMAIN_ENDPOINT_CLIENT = IDomainEndpoint.newInstance(PROPERTY_SERVICE);

    @Nullable
    private static String adminToken;

    @BeforeClass
    public static void setUp() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(PROPERTY_SERVICE.getAdminLogin());
        loginRequest.setPassword(PROPERTY_SERVICE.getAdminPassword());
        adminToken = AUTH_ENDPOINT_CLIENT.login(loginRequest).getToken();
    }

    @Test
    public void loadDataBackup() {
        @NotNull final DataBackupLoadRequest request = new DataBackupLoadRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.loadDataBackup(request));
    }

    @Test
    public void saveDataBackup() {
        @NotNull final DataBackupSaveRequest request = new DataBackupSaveRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.saveDataBackup(request));
    }

    @Test
    public void loadDataBase64() {
        @NotNull final DataBase64LoadRequest request = new DataBase64LoadRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.loadDataBase64(request));
    }

    @Test
    public void saveDataBase64() {
        @NotNull final DataBase64SaveRequest request = new DataBase64SaveRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.saveDataBase64(request));
    }

    @Test
    public void loadDataBinary() {
        @NotNull final DataBinaryLoadRequest request = new DataBinaryLoadRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.loadDataBinary(request));
    }

    @Test
    public void saveDataBinary() {
        @NotNull final DataBinarySaveRequest request = new DataBinarySaveRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.saveDataBinary(request));
    }

    @Test
    public void loadDataJsonFasterXml() {
        @NotNull final DataJsonLoadFasterXmlRequest request = new DataJsonLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.loadDataJsonFasterXml(request));
    }

    @Test
    public void saveDataJsonFasterXml() {
        @NotNull final DataJsonSaveFasterXmlRequest request = new DataJsonSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.saveDataJsonFasterXml(request));
    }

    @Test
    public void loadDataJsonJaxB() {
        @NotNull final DataJsonLoadJaxBRequest request = new DataJsonLoadJaxBRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.loadDataJsonJaxB(request));
    }

    @Test
    public void saveDataJsonJaxB() {
        @NotNull final DataJsonSaveJaxBRequest request = new DataJsonSaveJaxBRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.saveDataJsonJaxB(request));
    }

    @Test
    public void loadDataXmlFasterXml() {
        @NotNull final DataXmlLoadFasterXmlRequest request = new DataXmlLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.loadDataXmlFasterXml(request));
    }

    @Test
    public void saveDataXmlFasterXml() {
        @NotNull final DataXmlSaveFasterXmlRequest request = new DataXmlSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.saveDataXmlFasterXml(request));
    }

    @Test
    public void loadDataXmlJaxB() {
        @NotNull final DataXmlLoadJaxBRequest request = new DataXmlLoadJaxBRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.loadDataXmlJaxB(request));
    }

    @Test
    public void saveDataXmlJaxB() {
        @NotNull final DataXmlSaveJaxBRequest request = new DataXmlSaveJaxBRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.saveDataXmlJaxB(request));
    }

    @Test
    public void loadDataYamlFasterXml() {
        @NotNull final DataYamlLoadFasterXmlRequest request = new DataYamlLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.loadDataYamlFasterXml(request));
    }

    @Test
    public void saveDataYamlFasterXml() {
        @NotNull final DataYamlSaveFasterXmlRequest request = new DataYamlSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.saveDataYamlFasterXml(request));
    }

}