package ru.kuzin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataJsonSaveFasterXmlRequest extends AbstractUserRequest {

    public DataJsonSaveFasterXmlRequest(@Nullable final String token) {
        super(token);
    }

}