package ru.kuzin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.api.repository.IRepository;
import ru.kuzin.tm.enumerated.Sort;
import ru.kuzin.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    @NotNull
    List<M> findAll(@Nullable Sort sort);

}