package ru.kuzin.tm.repository;

import ru.kuzin.tm.api.repository.ISessionRepository;
import ru.kuzin.tm.model.Session;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {
}